<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Controllers\HouseController;
use App\Http\Controllers\ApartmentController;
use App\Http\Controllers\FurnitureController;
use App\Http\Controllers\FurnitureLogisticHistoryController;

Route::group([
    'prefix' => 'house'
], function () {
    Route::get('/', [HouseController::class, 'items'])
        ->name('house.items');
    Route::get('/{house}', [HouseController::class, 'item'])
        ->name('house.item');
    Route::get('/{house}/apartments', [HouseController::class, 'apartments'])
        ->name('house.apartments');
});

Route::group([
    'prefix' => 'apartment'
], function () {
    Route::get('/{apartment}', [ApartmentController::class, 'item'])
        ->name('house.item');
    Route::get('/{apartment}/rooms', [ApartmentController::class, 'rooms'])
        ->name('house.rooms');
});

Route::group([
    'prefix' => 'furniture'
], function () {
    Route::get('/', [FurnitureController::class, 'items'])
        ->name('furniture.items');
    Route::get('/{furniture}', [FurnitureController::class, 'item'])
        ->name('furniture.item');
    Route::get('/{furniture}/history', [FurnitureController::class, 'itemHistory'])
        ->name('furniture.itemHistory');
});

Route::group([
    'prefix' => 'furniture-logistic-history'
], function () {
    Route::get('/', [FurnitureLogisticHistoryController::class, 'items'])
        ->name('furniture-logistic-history.items');
    Route::get('/house/{house}', [FurnitureLogisticHistoryController::class, 'itemsByHouse'])
        ->name('furniture-logistic-history.itemsByHouse');
    Route::get('/apartment/{apartment}', [FurnitureLogisticHistoryController::class, 'itemsByApartment'])
        ->name('furniture-logistic-history.itemsByApartment');
    Route::get('/room/{room}', [FurnitureLogisticHistoryController::class, 'itemsByRoom'])
        ->name('furniture-logistic-history.itemsByRoom');
});
