<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * История логистики мебели.
 *
 * @property int $id
 * @property int $furniture_id
 * @property int $house_id
 * @property int $apartment_id
 * @property int $room_id
 * @property string $date_import (Дата ввоза)
 * @property string $date_export (Дата вывоза)
 * @property Furniture $furniture
 * @property House $house
 * @property Apartment $apartment
 * @property Room $room
 */
class FurnitureLogisticHistory extends Model
{
    use HasFactory;

    protected $table = 'furniture_logistic_history';

    public function furniture(): BelongsTo
    {
        return $this->belongsTo(Furniture::class);
    }

    public function house(): BelongsTo
    {
        return $this->belongsTo(House::class);
    }

    public function apartment(): BelongsTo
    {
        return $this->belongsTo(Apartment::class);
    }

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class);
    }
}
