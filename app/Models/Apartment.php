<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Квартира/корпус дома.
 *
 * @property int $id
 * @property int $house_id
 * @property string $number
 * @property House $house
 * @property Collection|Room[] $rooms
 * @property Collection|FurnitureLogisticHistory[] $furnitureLogisticHistory
 */
class Apartment extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function house(): BelongsTo
    {
        return $this->belongsTo(House::class);
    }

    public function rooms(): HasMany
    {
        return $this->hasMany(Room::class);
    }

    public function furnitureLogisticHistory(): HasMany
    {
        return $this->hasMany(FurnitureLogisticHistory::class);
    }
}
