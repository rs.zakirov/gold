<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Справочник мебели.
 *
 * @property int $id
 * @property string $name
 * @property int $furniture_type_id
 * @property FurnitureType $furnitureType
 * @property Collection|FurnitureLogisticHistory[] $furnitureLogisticHistory
 */
class Furniture extends Model
{
    use HasFactory;

    protected $table = 'furnitures';

    public $timestamps = false;

    public function furnitureType(): BelongsTo
    {
        return $this->belongsTo(FurnitureType::class);
    }

    public function furnitureLogisticHistory(): HasMany
    {
        return $this->hasMany(FurnitureLogisticHistory::class);
    }
}
