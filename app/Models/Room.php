<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Комната объекта недвижимости.
 *
 * @property int $id
 * @property int $apartment_id
 * @property int $room_type_id
 * @property Apartment $apartment
 * @property RoomType $roomType
 * @property Collection|FurnitureLogisticHistory[] $furnitureLogisticHistory
 */
class Room extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function apartment(): BelongsTo
    {
        return $this->belongsTo(Apartment::class);
    }

    public function roomType(): BelongsTo
    {
        return $this->belongsTo(RoomType::class);
    }

    public function furnitureLogisticHistory(): HasMany
    {
        return $this->hasMany(FurnitureLogisticHistory::class);
    }
}
