<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Дом/объект недвижимости.
 *
 * @property int $id
 * @property string $country
 * @property string $city
 * @property string $address
 * @property Collection|Apartment[] $apartments
 * @property Collection|FurnitureLogisticHistory[] $furnitureLogisticHistory
 */
class House extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function apartments(): HasMany
    {
        return $this->hasMany(Apartment::class);
    }

    public function furnitureLogisticHistory(): HasMany
    {
        return $this->hasMany(FurnitureLogisticHistory::class);
    }
}
