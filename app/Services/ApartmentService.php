<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Apartment;
use Illuminate\Pagination\LengthAwarePaginator;

final class ApartmentService
{
    public function itemsPaginate(): LengthAwarePaginator
    {
        return Apartment::paginate();
    }
}
