<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Apartment;
use App\Models\Furniture;
use App\Models\FurnitureLogisticHistory;
use App\Models\House;
use App\Models\Room;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

final class FurnitureLogisticHistoryService
{
    public function itemsPaginate(?string $date = null): LengthAwarePaginator
    {
        return
            FurnitureLogisticHistory::where(function ($query) use ($date) {
                if ($date !== null) {
                    $query
                        ->where('date_import', '<=', $date)
                        ->where('date_export', '>=', $date);
                }
            })->paginate();
    }

    public function itemsByFurnitureAndDate(Furniture $furniture, ?string $date = null): LengthAwarePaginator
    {
        return
            $furniture
                ->furnitureLogisticHistory()
                ->where(function ($query) use ($date) {
                    if ($date !== null) {
                        $query
                            ->where('date_import', '<=', $date)
                            ->where('date_export', '>=', $date);
                    }
                })
                ->paginate();
    }

    public function itemsByHouseAndDate(House $house, ?string $date = null): LengthAwarePaginator
    {
        return
            $house
                ->furnitureLogisticHistory()
                ->where(function ($query) use ($date) {
                    if ($date !== null) {
                        $query
                            ->where('date_import', '<=', $date)
                            ->where('date_export', '>=', $date);
                    }
                })
                ->paginate();
    }

    public function itemsByApartmentAndDate(Apartment $apartment, ?string $date = null): LengthAwarePaginator
    {
        return
            $apartment
                ->furnitureLogisticHistory()
                ->where(function ($query) use ($date) {
                    if ($date !== null) {
                        $query
                            ->where('date_import', '<=', $date)
                            ->where('date_export', '>=', $date);
                    }
                })
                ->paginate();
    }

    public function itemsByRoomAndDate(Room $room, ?string $date = null): LengthAwarePaginator
    {
        return
            $room
                ->furnitureLogisticHistory()
                ->where(function ($query) use ($date) {
                    if ($date !== null) {
                        $query
                            ->where('date_import', '<=', $date)
                            ->where('date_export', '>=', $date);
                    }
                })
                ->paginate();
    }
}
