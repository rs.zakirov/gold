<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\House;
use Illuminate\Pagination\LengthAwarePaginator;

final class HouseService
{
    public function itemsPaginate(): LengthAwarePaginator
    {
        return House::paginate();
    }
}
