<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Furniture;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

final class FurnitureService
{
    public function itemsPaginate(): LengthAwarePaginator
    {
        return Furniture::paginate();
    }
}
