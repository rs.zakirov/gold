<?php

declare(strict_types=1);

namespace App\Http\Responses;

use Symfony\Component\HttpFoundation\Response;

final class JsonApiSuccessResponse extends Response
{
    public static function createEmpty(int $status = Response::HTTP_OK, array $headers = []): self
    {
        return (new self(
            '',
            $status,
            self::fillHeaders($headers)
        ));
    }

    public static function createFromEntity(\JsonSerializable $entity, int $status = Response::HTTP_OK, array $headers = []): self
    {
        return (new self(
            self::transformContent($entity),
            $status,
            self::fillHeaders($headers)
        ));
    }

    private static function transformContent(\JsonSerializable $entity): string
    {
        return json_encode([
            'meta' => [
                'success' => true
            ],
            'data' => $entity->jsonSerialize()
        ]);
    }

    private static function fillHeaders(array $headers = []): array
    {
        if (empty($headers['Content-Type'])) {
            $headers['Content-Type'] = 'application/json';
        }

        return $headers;
    }
}
