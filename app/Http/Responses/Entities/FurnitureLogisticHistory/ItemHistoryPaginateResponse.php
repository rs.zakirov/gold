<?php

declare(strict_types=1);

namespace App\Http\Responses\Entities\FurnitureLogisticHistory;

use App\Models\FurnitureLogisticHistory;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Responses\Entities\LengthAwarePaginatorTrait;

final class ItemHistoryPaginateResponse implements \JsonSerializable
{
    use LengthAwarePaginatorTrait;

    private LengthAwarePaginator $data;

    public function __construct(LengthAwarePaginator $data)
    {
        $this->data = $data;
    }

    public function jsonSerialize(): array
    {
        $data = $this->data;
        $output = $this->prepareOutput($data);
        $output['items'] = $data->collect()->map(function (FurnitureLogisticHistory $furnitureLogisticHistory) {
            $room = $furnitureLogisticHistory->room;
            $roomType = $room->roomType;
            $apartment = $furnitureLogisticHistory->apartment;
            $house = $furnitureLogisticHistory->house;

            return [
                'dateImport' => $furnitureLogisticHistory->date_import,
                'dateExport' => $furnitureLogisticHistory->date_export,
                'room' => [
                    'id' => $room->id,
                    'apartment' => [
                        'id' => $apartment->id,
                        'number' => $apartment->number,
                        'house' => [
                            'id' => $house->id,
                            'country' => $house->country,
                            'city' => $house->city,
                            'address' => $house->address,
                        ]
                    ],
                    'roomType' => [
                        'id' => $roomType->id,
                        'name' => $roomType->name,
                    ]
                ],
            ];
        });

        return $output;
    }
}
