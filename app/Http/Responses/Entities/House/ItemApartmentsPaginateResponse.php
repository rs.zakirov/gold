<?php

declare(strict_types=1);

namespace App\Http\Responses\Entities\House;

use App\Models\Apartment;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Responses\Entities\LengthAwarePaginatorTrait;

final class ItemApartmentsPaginateResponse implements \JsonSerializable
{
    use LengthAwarePaginatorTrait;

    private LengthAwarePaginator $data;

    public function __construct(LengthAwarePaginator $data)
    {
        $this->data = $data;
    }

    public function jsonSerialize(): array
    {
        $data   = $this->data;
        $output = $this->prepareOutput($data);
        $output['items'] = $data->collect()->map(function (Apartment $apartment) {
            return [
                'id' => $apartment->id,
                'number' => $apartment->number,
            ];
        });

        return $output;
    }
}
