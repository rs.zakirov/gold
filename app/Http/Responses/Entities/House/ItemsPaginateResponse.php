<?php

declare(strict_types=1);

namespace App\Http\Responses\Entities\House;

use App\Models\House;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Responses\Entities\LengthAwarePaginatorTrait;

final class ItemsPaginateResponse implements \JsonSerializable
{
    use LengthAwarePaginatorTrait;

    private LengthAwarePaginator $data;

    public function __construct(LengthAwarePaginator $data)
    {
        $this->data = $data;
    }

    public function jsonSerialize(): array
    {
        $data   = $this->data;
        $output = $this->prepareOutput($data);
        $output['items'] = $data->collect()->map(function (House $house) {
            return [
                'id' => $house->id,
                'country' => $house->country,
                'city' => $house->city,
                'address' => $house->address,
            ];
        });

        return $output;
    }
}
