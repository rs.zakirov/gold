<?php

declare(strict_types=1);

namespace App\Http\Responses\Entities\House;

use App\Models\House;

final class ItemResponse implements \JsonSerializable
{
    private House $house;

    public function __construct(House $house)
    {
        $this->house = $house;
    }

    public function jsonSerialize(): array
    {
        $house = $this->house;

        return [
            'id' => $house->id,
            'country' => $house->country,
            'city' => $house->city,
            'address' => $house->address,
        ];
    }
}
