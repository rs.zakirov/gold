<?php

declare(strict_types=1);

namespace App\Http\Responses\Entities\Furniture;

use App\Models\Furniture;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Responses\Entities\LengthAwarePaginatorTrait;

final class ItemsResponse implements \JsonSerializable
{
    use LengthAwarePaginatorTrait;

    private LengthAwarePaginator $data;

    public function __construct(LengthAwarePaginator $data)
    {
        $this->data = $data;
    }

    public function jsonSerialize(): array
    {
        $data = $this->data;
        $output = $this->prepareOutput($data);
        $output['items'] = $this->data->map(function (Furniture $furniture) {
            return [
                'id' => $furniture->id,
                'name' => $furniture->name,
                'furnitureType' => [
                    'id' => $furniture->furnitureType->id,
                    'name' => $furniture->furnitureType->name,
                ]
            ];
        })->toArray();

        return $output;
    }
}
