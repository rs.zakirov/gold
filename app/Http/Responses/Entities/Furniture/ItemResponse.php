<?php

declare(strict_types=1);

namespace App\Http\Responses\Entities\Furniture;

use App\Models\Furniture;

final class ItemResponse implements \JsonSerializable
{
    private Furniture $furniture;

    public function __construct(Furniture $furniture)
    {
        $this->furniture = $furniture;
    }

    public function jsonSerialize(): array
    {
        $furniture = $this->furniture;

        return [
            'id' => $furniture->id,
            'name' => $furniture->name,
            'furnitureType' => [
                'id' => $furniture->furnitureType->id,
                'name' => $furniture->furnitureType->name,
            ]
        ];
    }
}
