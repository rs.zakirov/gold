<?php

declare(strict_types=1);

namespace App\Http\Responses\Entities\Apartment;

use App\Models\Room;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Responses\Entities\LengthAwarePaginatorTrait;

final class ItemRoomsPaginateResponse implements \JsonSerializable
{
    use LengthAwarePaginatorTrait;

    private LengthAwarePaginator $data;

    public function __construct(LengthAwarePaginator $data)
    {
        $this->data = $data;
    }

    public function jsonSerialize(): array
    {
        $data = $this->data;
        $output = $this->prepareOutput($data);
        $output['items'] = $data->collect()->map(function (Room $room) {
            $apartment = $room->apartment;
            $house = $apartment->house;
            $roomType = $room->roomType;

            return [
                'id' => $room->id,
                'apartment' => [
                    'id' => $apartment->id,
                    'house' => [
                        'id' => $house->id,
                        'country' => $house->country,
                        'city' => $house->city,
                        'address' => $house->address,
                    ],
                    'number' => $apartment->number,
                ],
                'roomType' => [
                    'id' => $roomType->id,
                    'name' => $roomType->name,
                ]
            ];
        });

        return $output;
    }
}
