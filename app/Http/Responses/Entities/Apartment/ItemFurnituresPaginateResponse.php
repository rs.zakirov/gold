<?php

declare(strict_types=1);

namespace App\Http\Responses\Entities\Apartment;

use App\Models\FurnitureLogisticHistory;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Responses\Entities\LengthAwarePaginatorTrait;

final class ItemFurnituresPaginateResponse implements \JsonSerializable
{
    use LengthAwarePaginatorTrait;

    private LengthAwarePaginator $data;

    public function __construct(LengthAwarePaginator $data)
    {
        $this->data = $data;
    }

    public function jsonSerialize(): array
    {
        $data = $this->data;
        $output = $this->prepareOutput($data);
        $output['items'] = $data->collect()->map(function (FurnitureLogisticHistory $furnitureLogisticHistory) {
            $furniture = $furnitureLogisticHistory->furniture;
            $furnitureType = $furniture->furnitureType;

            return [
                'dateImport' => $furnitureLogisticHistory->date_import,
                'dateExport' => $furnitureLogisticHistory->date_export,
                'furniture' => [
                    'id' => $furniture->id,
                    'name' => $furniture->name,
                    'furnitureType' => [
                        'id' => $furnitureType->id,
                        'name' => $furnitureType->name,
                    ]
                ],
            ];
        });

        return $output;
    }
}
