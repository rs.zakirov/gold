<?php

declare(strict_types=1);

namespace App\Http\Responses\Entities\Apartment;

use App\Models\Apartment;

final class ItemResponse implements \JsonSerializable
{
    private Apartment $apartment;

    public function __construct(Apartment $apartment)
    {
        $this->apartment = $apartment;
    }

    public function jsonSerialize(): array
    {
        $apartment = $this->apartment;
        $house = $apartment->house;

        return [
            'id' => $apartment->id,
            'number' => $apartment->number,
            'house' => [
                'id' => $house->id,
                'country' => $house->country,
                'city' => $house->city,
                'address' => $house->address,
            ],
        ];
    }
}
