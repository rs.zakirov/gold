<?php

declare(strict_types=1);

namespace App\Http\Responses\Entities;

use Illuminate\Pagination\LengthAwarePaginator;

trait LengthAwarePaginatorTrait
{
    public function prepareOutput(LengthAwarePaginator $data): array
    {
        return [
            'total' => $data->total(),
            'lastPage' => $data->lastPage(),
            'perPage' => $data->perPage(),
            'currentPage' => $data->currentPage(),
        ];
    }
}
