<?php

namespace App\Http\Controllers;

use App\Models\Furniture;
use Illuminate\Http\Request;
use App\Services\FurnitureService;
use App\Http\Responses\JsonApiSuccessResponse;
use App\Services\FurnitureLogisticHistoryService;
use App\Http\Responses\Entities\Furniture\ItemResponse;
use App\Http\Responses\Entities\Furniture\ItemsResponse;
use App\Http\Responses\Entities\Furniture\ItemHistoryResponse;

class FurnitureController extends Controller
{
    private FurnitureService $furnitureService;

    private FurnitureLogisticHistoryService $furnitureLogisticHistoryService;

    public function __construct(
        FurnitureService $furnitureService,
        FurnitureLogisticHistoryService $furnitureLogisticHistoryService)
    {
        $this->furnitureService = $furnitureService;
        $this->furnitureLogisticHistoryService = $furnitureLogisticHistoryService;
    }

    public function items(): JsonApiSuccessResponse
    {
        return JsonApiSuccessResponse::createFromEntity(
            new ItemsResponse(
                $this->furnitureService->itemsPaginate()
            )
        );
    }

    public function item(Furniture $furniture): JsonApiSuccessResponse
    {
        return JsonApiSuccessResponse::createFromEntity(
            new ItemResponse(
                $furniture
            )
        );
    }

    public function itemHistory(Furniture $furniture, Request $request): JsonApiSuccessResponse
    {
        $furnitureLogisticHistoryService = $this->furnitureLogisticHistoryService;

        return JsonApiSuccessResponse::createFromEntity(
            new ItemHistoryResponse(
                $furnitureLogisticHistoryService->itemsByFurnitureAndDate(
                    $furniture,
                    $request->get('date')
                )
            )
        );
    }
}
