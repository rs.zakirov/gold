<?php

namespace App\Http\Controllers;

use App\Models\Apartment;
use App\Services\ApartmentService;
use App\Http\Responses\JsonApiSuccessResponse;
use App\Services\FurnitureLogisticHistoryService;
use App\Http\Responses\Entities\Apartment\ItemResponse;
use App\Http\Responses\Entities\Apartment\ItemRoomsPaginateResponse;

class ApartmentController extends Controller
{
    private ApartmentService $apartmentService;

    private FurnitureLogisticHistoryService $furnitureLogisticHistoryService;

    public function __construct(
        ApartmentService $apartmentService,
        FurnitureLogisticHistoryService $furnitureLogisticHistoryService)
    {
        $this->apartmentService = $apartmentService;
        $this->furnitureLogisticHistoryService = $furnitureLogisticHistoryService;
    }

    public function item(Apartment $apartment): JsonApiSuccessResponse
    {
        return JsonApiSuccessResponse::createFromEntity(
            new ItemResponse($apartment)
        );
    }

    public function rooms(Apartment $apartment): JsonApiSuccessResponse
    {
        return JsonApiSuccessResponse::createFromEntity(
            new ItemRoomsPaginateResponse($apartment->rooms()->paginate())
        );
    }
}
