<?php

namespace App\Http\Controllers;

use App\Models\House;
use App\Services\HouseService;
use App\Http\Responses\JsonApiSuccessResponse;
use App\Http\Responses\Entities\House\ItemResponse;
use App\Http\Responses\Entities\House\ItemApartmentsPaginateResponse;
use App\Http\Responses\Entities\House\ItemsPaginateResponse;

class HouseController extends Controller
{
    private HouseService $houseService;

    public function __construct(HouseService $houseService)
    {
        $this->houseService = $houseService;
    }

    public function items(): JsonApiSuccessResponse
    {
        return JsonApiSuccessResponse::createFromEntity(
            new ItemsPaginateResponse($this->houseService->itemsPaginate())
        );
    }

    public function item(House $house): JsonApiSuccessResponse
    {
        return JsonApiSuccessResponse::createFromEntity(
            new ItemResponse($house)
        );
    }

    public function apartments(House $house): JsonApiSuccessResponse
    {
        return JsonApiSuccessResponse::createFromEntity(
            new ItemApartmentsPaginateResponse($house->apartments()->paginate())
        );
    }
}
