<?php

namespace App\Http\Controllers;

use App\Http\Responses\Entities\Furniture\ItemHistoryResponse;
use App\Models\Room;
use App\Models\House;
use App\Models\Apartment;
use App\Http\Responses\JsonApiSuccessResponse;
use App\Services\FurnitureLogisticHistoryService;
use App\Http\Responses\Entities\FurnitureLogisticHistory\ItemsHistoryPaginateResponse;
use Illuminate\Http\Request;

class FurnitureLogisticHistoryController extends Controller
{
    private FurnitureLogisticHistoryService $furnitureLogisticHistoryService;

    public function __construct(FurnitureLogisticHistoryService $furnitureLogisticHistoryService)
    {
        $this->furnitureLogisticHistoryService = $furnitureLogisticHistoryService;
    }

    public function items(Request $request): JsonApiSuccessResponse
    {
        return JsonApiSuccessResponse::createFromEntity(
            new ItemsHistoryPaginateResponse(
                $this->furnitureLogisticHistoryService->itemsPaginate(
                    $request->get('date')
                )
            )
        );
    }

    public function itemsByHouse(House $house, Request $request): JsonApiSuccessResponse
    {
        $furnitureLogisticHistoryService = $this->furnitureLogisticHistoryService;

        return JsonApiSuccessResponse::createFromEntity(
            new ItemHistoryResponse(
                $furnitureLogisticHistoryService->itemsByHouseAndDate(
                    $house,
                    $request->get('date')
                )
            )
        );
    }

    public function itemsByApartment(Apartment $apartment, Request $request): JsonApiSuccessResponse
    {
        $furnitureLogisticHistoryService = $this->furnitureLogisticHistoryService;

        return JsonApiSuccessResponse::createFromEntity(
            new ItemHistoryResponse(
                $furnitureLogisticHistoryService->itemsByApartmentAndDate(
                    $apartment,
                    $request->get('date')
                )
            )
        );
    }

    public function itemsByRoom(Room $room, Request $request): JsonApiSuccessResponse
    {
        $furnitureLogisticHistoryService = $this->furnitureLogisticHistoryService;

        return JsonApiSuccessResponse::createFromEntity(
            new ItemHistoryResponse(
                $furnitureLogisticHistoryService->itemsByRoomAndDate(
                    $room,
                    $request->get('date')
                )
            )
        );
    }
}
