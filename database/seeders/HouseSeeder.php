<?php

namespace Database\Seeders;

use App\Models\Apartment;
use App\Models\House;
use App\Models\Room;
use Illuminate\Database\Seeder;

class HouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        House::factory()
            ->count(10)
            ->has(
                Apartment::factory()
                    ->has(
                        Room::factory()
                            ->count(4)
                    )
                    ->count(50)
            )
            ->create();
    }
}
