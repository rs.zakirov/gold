<?php

namespace Database\Seeders;

use App\Models\FurnitureType;
use Illuminate\Database\Seeder;

class FurnitureTypeSeeder extends Seeder
{
    private const DATA = [
        'Стол',
        'Стул',
        'Кровать',
        'Диван',
        'Шкаф',
        'Кресло',
        'Гамак',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];

        foreach (self::DATA as $value) {
            $data[] = [
                'name' => $value
            ];
        }

        FurnitureType::insert($data);
    }
}
