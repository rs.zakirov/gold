<?php

namespace Database\Seeders;

use App\Models\Furniture;
use App\Models\FurnitureLogisticHistory;
use App\Models\House;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class FurnitureLogisticHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $houses = House::all();
        $furnitures = Furniture::all()->take(50);

        /** @var House $house */
        foreach ($houses as $house) {
            $apartments = $house->apartments;

            foreach ($apartments as $apartment) {
                $rooms = $apartment->rooms;

                foreach ($rooms as $room) {
                    $data[] = [
                        'room_id' => $room->id,
                        'house_id' => $house->id,
                        'furniture_id' => $furnitures->random()->id,
                        'apartment_id' => $apartment->id,
                        'date_import' => Carbon::now()->subDays(rand(0, 30)),
                        'date_export' => Carbon::now()->addDays(rand(-2, 30)),
                    ];
                }
            }
        }

        FurnitureLogisticHistory::insert($data);
    }
}
