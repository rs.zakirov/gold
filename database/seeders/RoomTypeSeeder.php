<?php

namespace Database\Seeders;

use App\Models\RoomType;
use Illuminate\Database\Seeder;

class RoomTypeSeeder extends Seeder
{
    private const DATA = [
        'Кухня',
        'Ванная комнана',
        'Прихожая',
        'Гостинная',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];

        foreach (self::DATA as $value) {
            $data[] = [
                'name' => $value
            ];
        }

        RoomType::insert($data);
    }
}
