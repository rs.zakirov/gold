<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFurnitureLogisticHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('furniture_logistic_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('furniture_id');
            $table->bigInteger('house_id');
            $table->bigInteger('apartment_id');
            $table->bigInteger('room_id');
            $table->date('date_import');
            $table->date('date_export');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('furniture_logistic_history');
    }
}
