<?php

namespace Database\Factories;

use App\Models\FurnitureType;
use Illuminate\Database\Eloquent\Factories\Factory;

class FurnitureFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(50),
            'furniture_type_id' => FurnitureType::all()->random()->id
        ];
    }
}
