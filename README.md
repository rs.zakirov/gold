## Gold project

Список команд для запуска проекта:

В первую очередь скопируйте .env.example в .env. Если у вас Linux, убедитесь, что ваш uid совпадает со значением переменной USER_ID.

1. Загрузите образа и на их основе создайте контейнеры:

    ```
   docker-compose up -d
    ```
1. Убедитесь что все контейнеры запустились успешно:

    ```
   docker-compose ps
    ```
1. Установите зависимости:

    ```
   composer install
    ```
1. Выполните миграции:

    ```
   php artisan migrate
    ```
1. И сиды:

    ```
   php artisan db:seed
    ```

Если всё прошло успешно, сайт доступен по адресу: http://localhost

### Описание маршрутов

1. localhost/api/house (список объектов недвижимости)
   
   localhost/api/house/[id] (информация об объекте)
   
    localhost/api/house/[id]/apartments (список квартир объекта)

1. localhost/api/apartment/[id] (информация о квартире)
   
   localhost/api/apartment/[id]/rooms (список комнат в квартире)

1. localhost/api/furniture (справочник мебели)
   
   localhost/api/furniture/[id] (информация по мебели)
   
    localhost/api/furniture/[id]/history (история логистики мебельного предмета)
   
    localhost/api/furniture/[id]/history?date=YYYY-MM-DD (тоже самое, только по дате)

1. localhost/api/furniture-logistic-history (вся история логистики)
   
   localhost/api/furniture-logistic-history?date=YYYY-MM-DD (тоже самое по дате)
   
    localhost/api/furniture-logistic-history/house/[id]?date=YYYY-MM-DD (по дате и дому)
   
    localhost/api/furniture-logistic-history/apartment/[id]?date=YYYY-MM-DD (по дате и квартире)
   
    localhost/api/furniture-logistic-history/room/[id]?date=YYYY-MM-DD (по дате и комнате)
